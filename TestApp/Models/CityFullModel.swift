//
//  CityFullModel.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation

struct CityFullArray: Codable {
    let array: [CityFull]
    enum CodingKeys: String, CodingKey {
        case array = "list"
    }
}

struct CityFull: Codable {
    var id: Int
    var name: String?
    var main: Main?
    var wind: Wind?
}

struct Main: Codable {
    var temperature: Float?
    enum CodingKeys: String, CodingKey {
        case temperature = "temp"
    }
}

struct Wind: Codable {
    var speed: Float?
    var degree: Float?
    enum CodingKeys: String, CodingKey {
        case speed
        case degree = "deg"
    }
}


