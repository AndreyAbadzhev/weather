//
//  CityShortModel.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import RealmSwift

class CityShortArray: Codable {
    var cities: Array<CityShort> = []
}

class CityShort: Object, Codable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    
    required convenience init(id: Int, name: String) {
        self.init()
        self.id = id
        self.name = name
    }
}
