//
//  AlertConstructor.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import UIKit

class AlertConstructor {
    
    func errorAlert(error: ApiError) -> UIAlertController {
        let alert = UIAlertController(title: "Oops", message: error.rawValue, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAlertAction)
        return alert
    }

    
    func alertAboutYourLocation(cityName: String) -> UIAlertController {
        let alert = UIAlertController(title: nil, message: "It seems, you are in \(cityName)", preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAlertAction)
        return alert
    }
}
