//
//  CitiesRouterInput.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
protocol CitiesRouterInput {
    func showAddCitiesViewController()
}
