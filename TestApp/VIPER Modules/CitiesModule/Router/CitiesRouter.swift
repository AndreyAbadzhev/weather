//
//  CitiesRouter.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
class CitiesRouter: CitiesRouterInput {
    
    weak var view: CitiesViewController?
    
    init(view: CitiesViewController?) {
        self.view = view
    }
    
    func showAddCitiesViewController() {
        let selectCitiesViewController = SelectCitiesViewController()
        let selectCitiesConfigurator = SelectCitiesConfigurator(view: selectCitiesViewController)
        selectCitiesConfigurator.configure()
        
        view?.navigationController?.pushViewController(selectCitiesViewController, animated: true)
    }
    
}
