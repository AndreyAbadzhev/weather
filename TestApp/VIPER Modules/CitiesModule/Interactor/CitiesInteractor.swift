//
//  CitiesInteractor.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import Alamofire
class CitiesInteractor: CitiesInteractorInput, UserDefaultsProtocol {

    let api: WeatherApiService = ServerRequests()
    
    func performServerRequest(noCitiesChosen: @escaping() -> (),
                              success: @escaping(CityFullArray) -> (),
                              failure: @escaping(ApiError) -> ()) {
        
        guard let citiesIdsArray = _chosenCitiesIds, !citiesIdsArray.isEmpty else { noCitiesChosen(); return }
        let citiesIdsStringArray = citiesIdsArray.map { String($0) }
        let citiesIdsString = citiesIdsStringArray.reduce("", { $0 == "" ? $1 : $0 + "," + $1 } )
        
        let parameters = ["id": citiesIdsString, "units": "metric", "appid": Constants.openWeatherApiKey]
    
        api.performWeatherRequest(parameters: parameters, success: success, failure: failure)
    }
    
    func removeCity(cityId: Int) {
        guard let chosenCitiesIds = _chosenCitiesIds else { return }
        let updatedChosenCitiesIds = chosenCitiesIds.filter { $0 != cityId }
        _writeCitiesIds(array: updatedChosenCitiesIds)
    }
    
}
