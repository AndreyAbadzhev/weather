//
//  CitiesPresenter.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
class CitiesPresenter: CitiesViewControllerOutput {
    
    weak var view: CitiesViewControllerInput?
    var interactor: CitiesInteractorInput?
    var router: CitiesRouterInput?
    
    var citiesFullArray: [CityFull]? = []
    
    var citiesCount: Int {
        return citiesFullArray?.count ?? 0
    }
    
    func viewDidLoad() {
        view?.setupInitialState()
    }
    
    func viewIsReady() {
        performServerRequest { [weak self] cities, error in
            if error != nil { self?.view?.showErrorAlert(error: error!) }
            self?.citiesFullArray = cities ?? []
            self?.view?.updateViewState()
        }
    }
    
    func performServerRequest(completion: @escaping([CityFull]?, ApiError?) -> ()) {
        interactor?.performServerRequest(
            noCitiesChosen: { completion(nil, nil) },
            success: { cityFullArray in completion(cityFullArray.array, nil) },
            failure: { error in completion(nil, error) }
        )
    }

    func addButtonDidTapped() {
        router?.showAddCitiesViewController()
    }
    
    func getCityForIndexPath(indexPath: IndexPath) -> CityFull? {
        return citiesFullArray?.getElement(index: indexPath.row)
    }
    
    func removeCity(cityId: Int) {
        citiesFullArray = citiesFullArray?.filter { $0.id != cityId }
        interactor?.removeCity(cityId: cityId)
    }
}
