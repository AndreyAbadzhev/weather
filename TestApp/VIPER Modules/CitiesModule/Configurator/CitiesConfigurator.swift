//
//  CitiesConfigurator.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
class CitiesConfigurator {
    
    weak var view: CitiesViewController?

    init(view: CitiesViewController) {
        self.view = view
    }
    
    func configure() {
        let presenter = CitiesPresenter()
        let interactor = CitiesInteractor()
        let router = CitiesRouter(view: view)
        
        view?.output = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
    }
    
}
