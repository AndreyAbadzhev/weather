//
//  CitiesViewController.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import UIKit
import CoreLocation
class CitiesViewController: UIViewController, CitiesViewControllerInput {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noCitiesChosenLabel: UILabel!
    
    var output: CitiesViewControllerOutput?
    
    let locationManager = CLLocationManager()
    var locationNeedsUpdate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareViewsForReuse()
        output?.viewIsReady()
    }
    
    func setupInitialState() {
        title = "Cities"
        prepareTableView()
        prepareNavigationBar()
        locationInitialize()
        getLocation()
    }
    
    func prepareViewsForReuse() {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        tableView.alpha = 0
        noCitiesChosenLabel.alpha = 0
    }

    func prepareTableView() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshWeatherData), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    func prepareNavigationBar() {
        let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonDidTapped))
        navigationItem.rightBarButtonItem = addBarButton
        tableView.register(UINib(nibName: String(describing: CitiesCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: CitiesCell.self))
    }
    
    func updateViewState() {
        activityIndicator.isHidden = true
        tableView.refreshControl?.endRefreshing()
        tableView.reloadData()
        output?.citiesCount == 0 ? hideTableView() : showTableView()
    }
    
    func showTableView() {
        tableView.animatedShow()
        noCitiesChosenLabel.animatedHide()
    }
    
    func hideTableView() {
        tableView.animatedHide()
        noCitiesChosenLabel.animatedShow()
    }
    
    @objc func addButtonDidTapped() {
        output?.addButtonDidTapped()
    }
    
    @objc func refreshWeatherData() {
        output?.viewIsReady()
    }
    
    func showErrorAlert(error: ApiError) {
        let alert = AlertConstructor().errorAlert(error: error)
        present(alert, animated: true, completion: nil)
    }
}

extension CitiesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.citiesCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let city = output?.getCityForIndexPath(indexPath: indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CitiesCell.self)) as? CitiesCell
            else { return UITableViewCell() }
        
        cell.configure(city: city)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let cell = tableView.cellForRow(at: indexPath) as? CitiesCell else { return }
            guard let city = cell.city else { return }
            output?.removeCity(cityId: city.id)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            if tableView.numberOfRows(inSection: 0) == 0 { hideTableView() }
        }
    }
}

extension CitiesViewController: CLLocationManagerDelegate {
    
    func locationInitialize() {
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
    }
    
    func getLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locationNeedsUpdate { return }
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        locationManager.stopUpdatingLocation()
        self.locationNeedsUpdate = false
        
        let coordinates = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(coordinates, completionHandler: { placemarks, error -> Void in
            guard let placeMark = placemarks?.first else { return }
            if let cityName = placeMark.subAdministrativeArea {
                let alert = AlertConstructor().alertAboutYourLocation(cityName: cityName)
                UIApplication.getTopMostViewController()?.present(alert, animated: true, completion: nil)
            }
        })
    }
}
