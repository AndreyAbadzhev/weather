//
//  CitiesViewControllerOutput.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
protocol CitiesViewControllerOutput {
    var citiesCount: Int { get }
    
    func viewDidLoad()
    func viewIsReady()
    
    func addButtonDidTapped()
    
    func getCityForIndexPath(indexPath: IndexPath) -> CityFull?
    func removeCity(cityId: Int)
}
