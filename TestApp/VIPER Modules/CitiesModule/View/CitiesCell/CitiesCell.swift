//
//  CitiesCell.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import UIKit

class CitiesCell: UITableViewCell {

    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
 
    var city: CityFull?
    
    func configure(city: CityFull) {
        self.city = city
        
        cityNameLabel.text = city.name
        
        temperatureLabel.text = city.main?.temperature != nil ? "Temperature: " + String(city.main?.temperature ?? 0) + "°" : "Temperature: no information"
        windSpeedLabel.text = city.wind?.speed != nil ? "Wind speed - " + String(city.wind?.speed ?? 0) + " meters per sec." : "Wind speed - no information"
        
        if let windDegree = city.wind?.degree {
            var windDirectionString = ""
            switch Double(windDegree) {
            case 337.6...360, 0...22.5: windDirectionString = "North"
            case 22.6...67.5: windDirectionString = "North East"
            case 67.6...112.5: windDirectionString = "East"
            case 112.6...157.5: windDirectionString = "South East"
            case 157.6...202.5: windDirectionString = "South"
            case 202.6...247.5: windDirectionString = "South West"
            case 247.6...292.5: windDirectionString = "West"
            case 292.6...337.5: windDirectionString = "North West"
            default: break
            }
            windDirectionLabel.text = "Wind direction - " + windDirectionString
        } else {
            windDirectionLabel.text = "Wind direction - no information"
        }
        
        
        
        
        
        //337.6 - 22.5 север
        //22.6 - 67.5 северо-восток
        //67.6 - 112.5 восток
        //112.6 - 157.5 юго-восток
        //157.6 - 202.5 юг
        //202.6 - 247.5 юго-запад
        //247.6 - 292.5 запад
        //292.6 - 337.5 северо-запад
    }
    
}
