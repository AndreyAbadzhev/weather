//
//  SelectCitiesConfigurator.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
class SelectCitiesConfigurator {
    
    weak var view: SelectCitiesViewController?
    
    init(view: SelectCitiesViewController) {
        self.view = view
    }
    
    func configure() {
        let presenter = SelectCitiesPresenter()
        let interactor = SelectCitiesInteractor()
        let router = SelectCitiesRouter(view: view)
        
        view?.output = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
    }
    
    deinit {
        print("SC Configurator deallocated")
    }
}
