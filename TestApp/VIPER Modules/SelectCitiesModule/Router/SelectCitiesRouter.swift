//
//  SelectCitiesRouter.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
class SelectCitiesRouter: SelectCitiesRouterInput {
    
    weak var view: SelectCitiesViewController?
    
    init(view: SelectCitiesViewController?) {
        self.view = view
    }
    
    func closeModule() {
        view?.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        print("SC Router deallocated")
    }
}
