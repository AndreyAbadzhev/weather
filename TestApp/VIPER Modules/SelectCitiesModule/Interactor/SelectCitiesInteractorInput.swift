//
//  SelectCitiesInteractorInput.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import RealmSwift
protocol SelectCitiesInteractorInput {
    func recoverSelectedCitiesFromDataStorage() -> [Int]?
    func writeSelectedCitiesToDataStorage(selectedCitiesIds: [Int], completion: @escaping() -> ())
}
