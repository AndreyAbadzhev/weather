//
//  SelectCitiesInteractor.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import RealmSwift
class SelectCitiesInteractor: SelectCitiesInteractorInput, UserDefaultsProtocol {

    weak var output: SelectCitiesInteractorOutput?
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: NSNotification.Name("__DataLoaded__"), object: nil)
    }
    
    func recoverSelectedCitiesFromDataStorage() -> [Int]? {
        return _chosenCitiesIds
    }
    
    func writeSelectedCitiesToDataStorage(selectedCitiesIds: [Int], completion: @escaping() -> ()) {
        _writeCitiesIds(array: selectedCitiesIds)
        completion()
    }
    
    deinit {
        print("SC Interactor deallocated")
    }
    
    
    @objc func updateData() {
        output?.update()
    }
}
