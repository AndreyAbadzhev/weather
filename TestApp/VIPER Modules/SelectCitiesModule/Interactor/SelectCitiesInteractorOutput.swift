//
//  SelectCitiesInteractorOutput.swift
//  TestApp
//
//  Created by Андрей Абаджев on 16/08/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
protocol SelectCitiesInteractorOutput: class {
    func update()
}
