//
//  SelectCitiesPresenter.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import RealmSwift
class SelectCitiesPresenter: SelectCitiesViewControllerOutput, SelectCitiesInteractorOutput {
    
    weak var view: SelectCitiesViewControllerInput?
    var interactor: SelectCitiesInteractorInput?
    var router: SelectCitiesRouterInput?
    
    var searchInProgress: Bool = false
    var previewLoading: Bool = true
    
    var selectedCitiesIds: [Int] = []
    
    var citiesCount: Int {
        if previewLoading { return  DataSource.shared().citiesArrayPreview.count }
        
        return searchInProgress ? DataSource.shared().citiesSearchResult.count : DataSource.shared().citiesArray.count
    }
    
    func viewDidLoad() {
        recoverData()
        view?.setupInitialState()
    }
    
    func viewIsReady() {
        print("started loading full array")
        previewLoading = false
        view?.updateViewState()
        
    }
    
    func recoverData() {
        selectedCitiesIds = interactor?.recoverSelectedCitiesFromDataStorage() ?? []
    }
    
    func update() {
        view?.updateViewState()
    }
    
    func getCityForIndexPath(indexPath: IndexPath) -> CityShort? {
        if previewLoading { return DataSource.shared().citiesArrayPreview.getElement(index: indexPath.row) }
        
        if searchInProgress {
            return DataSource.shared().citiesSearchResult.getElement(index: indexPath.row)
        } else {
            return DataSource.shared().citiesArray.getElement(index: indexPath.row)
        }
    }
    
    
    func checkForSelection(cityId: Int) -> Bool {
        return selectedCitiesIds.contains(cityId) ? true : false
    }
    
    func searchForCity(searchString: String, completion: @escaping() -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            let array = DataSource.shared().citiesArray.filter { $0.name.lowercased().contains(searchString.lowercased()) }
            DataSource.shared().citiesSearchResult = array.map { CityShort(id: $0.id, name: $0.name) }
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    
    func cityCellDidTapped(cityId: Int, placeMark: @escaping() -> (), removeMark: @escaping() -> ()) {
        if checkForSelection(cityId: cityId) {
            for (index, id) in selectedCitiesIds.enumerated() {
                if id == cityId {
                    selectedCitiesIds.remove(at: index)
                    removeMark()
                    break
                }
            }
        } else {
            selectedCitiesIds.append(cityId)
            placeMark()
        }
    }
    
    func doneBarButtonDidTapped() {
        interactor?.writeSelectedCitiesToDataStorage(selectedCitiesIds: selectedCitiesIds, completion: { [weak self] in
            self?.router?.closeModule()
        })
    }
    
    deinit {
        print("SC Presenter deallocated")
    }
}
