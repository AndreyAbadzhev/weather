//
//  ShortCityCell.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import UIKit

class ShortCityCell: UITableViewCell {

    @IBOutlet weak var cityNameLabel: UILabel!
    var cityId: Int = 0
    
    var cityShort: CityShort?
    
    func configure(cityShort: CityShort) {
        self.cityShort = cityShort
        cityNameLabel.text = cityShort.name
        cityId = cityShort.id
    }
}
