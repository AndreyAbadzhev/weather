//
//  SelectCitiesViewOutput.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
protocol SelectCitiesViewControllerOutput {
    var citiesCount: Int { get }
    var searchInProgress: Bool { get set }
    var previewLoading: Bool { get set }
    
    func viewDidLoad()
    func viewIsReady()
    
    func getCityForIndexPath(indexPath: IndexPath) -> CityShort?
    func checkForSelection(cityId: Int) -> Bool
    
    func searchForCity(searchString: String, completion: @escaping() -> ())
    func cityCellDidTapped(cityId: Int, placeMark: @escaping() -> (), removeMark: @escaping() -> ())
    func doneBarButtonDidTapped()
}
