//
//  SelectCitiesViewController.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import UIKit

class SelectCitiesViewController: UIViewController, SelectCitiesViewControllerInput {
    
    @IBOutlet weak var cityInputSearchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    var tableViewNeedsToUpdate: Bool = false
    
    var doneBarButton: UIBarButtonItem?
    
    var output: SelectCitiesViewControllerOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output?.viewIsReady()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        super.viewDidDisappear(animated)
    }

    func setupInitialState() {
        prepareViews()
        prepareNavigationBar()
        prepareTableView()
        setupKeyboardObservers()
        print("started loading preview array")
    }
    
    func prepareViews() {
        title = "Select City"
    }
    
    func prepareNavigationBar() {
        doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneBarButtonDidTapped))
        navigationItem.rightBarButtonItem = doneBarButton
        doneBarButton?.isEnabled = false
    }
    
    func prepareTableView() {
        tableView.register(UINib(nibName: String(describing: ShortCityCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: ShortCityCell.self))
    }
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func updateViewState() {
        tableView.reloadData()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            self.tableViewBottomConstraint.constant = keyboardHeight - 48
            UIView.animate(withDuration: 0.25) { self.view.layoutIfNeeded() }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.tableViewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.25) { self.view.layoutIfNeeded() }
    }
    
    @objc func doneBarButtonDidTapped() {
        output?.doneBarButtonDidTapped()
    }
    
    deinit {
        print("SC VC deallocated")
    }
}

extension SelectCitiesViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        output?.searchInProgress = false
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = ""
        searchBar.resignFirstResponder()
        updateViewState()
    }

    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        tableViewNeedsToUpdate = true
        output?.searchInProgress = searchBar.text?.isEmpty == true ? false : true
        output?.searchInProgress == false
            ? DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: { self.updateViewState() })
            : output?.searchForCity(searchString: searchBar.text ?? "", completion: { [weak self] in self?.updateViewState() })
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension SelectCitiesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.citiesCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let city = output?.getCityForIndexPath(indexPath: indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ShortCityCell.self)) as? ShortCityCell else { return UITableViewCell() }
        
        cell.configure(cityShort: city)
        let cellIsChecked = output?.checkForSelection(cityId: city.id) ?? false
        cell.accessoryType = cellIsChecked ? .checkmark : .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ShortCityCell else { return }
        doneBarButton?.isEnabled = true
        let cityId = cell.cityId
        output?.cityCellDidTapped(cityId: cityId, placeMark: { [weak cell] in
            cell?.accessoryType = .checkmark
        }, removeMark: { [weak cell] in
            cell?.accessoryType = .none
        })
    }
}
