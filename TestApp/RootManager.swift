//
//  ViewController.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import UIKit

class RootManager: UserDefaultsProtocol {

    var window: UIWindow
    
    var pattern: PatternUse = .VIPER
    
    init(window: UIWindow) {
        self.window = window
        configureStart()
    }
    
    func configureStart() {
        fillRealmIfNeeded()
        createDataSource()
        showCitiesViewController()
    }
    
    func fillRealmIfNeeded() {
        if _firstLaunchCompleted == true { return }
        showSplashScreen()

        guard let path = Bundle.main.path(forResource: "city.list", ofType: "json") else {
            print("Файл JSON \"city.list\" отсутствует")
            return
        }
        
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe) else {
            print("Файл JSON по пути \(path) поврежден и не может быть конвертирован в словарь")
            return
        }
        
        
        do {
            let cityShortArray = try (JSONDecoder().decode(CityShortArray.self, from: data))
            let realm: RealmDataBase = RealmDataBase()
            realm.write(object: cityShortArray.cities)
            _firstLaunchSetCompleted()
        } catch {
            return
        }
    }
    
    func createDataSource() {
        DispatchQueue.global(qos: .userInteractive).async {
            let realm: RealmDataBase = RealmDataBase()
            guard let arrayResultRealm = realm.obtain(objectType: CityShort.self) else { return }

            DataSource.shared().citiesArray = arrayResultRealm.map { CityShort(id: $0.id, name: $0.name) }
            DataSource.shared().citiesArrayPreview = DataSource.shared().citiesArray.prefix(50).map { $0 }
            
            DispatchQueue.main.async { self.dataLoaded() }
        }
    }
    
    func showCitiesViewController() {
        pattern == .MVVM ? useMVVM() : useVIPER()
    }
    
    func useVIPER() {
        // MARK: VIPER
        let citiesViewController = CitiesViewController()
        let citiesConfigurator = CitiesConfigurator(view: citiesViewController)
        
        citiesConfigurator.configure()
        
        let navigationController = UINavigationController(rootViewController: citiesViewController)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    func useMVVM() {
        //MARK: MVVM
       /* let citiesViewController = MVVMCitiesView()
        
        let navigationController = UINavigationController(rootViewController: citiesViewController)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()*/
    }
    
    func dataLoaded() {
        NotificationCenter.default.post(Notification(name: Notification.Name("__DataLoaded__")))
    }
    
    func showSplashScreen() {
        let splashScreen = SplashScreenViewController()
        window.rootViewController = splashScreen
        window.makeKeyAndVisible()
    }
    
    deinit {
        print("SC Root Manager deallocated")
    }
}

enum PatternUse {
    case VIPER
    case MVVM
}
