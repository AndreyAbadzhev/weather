//
//  ApiCore.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
class ServerRequesterCore {
    
    func constructRequest(urlString: String,
                          parameters: [String: String],
                          method: HTTPMethod,
                          body: [String: Any]) -> URLRequest? {

        var _urlString = urlString
        
        for (key, value) in parameters {
            _urlString += key + "=" + value + "&"
        }
        
        guard let encodedUrlString = (_urlString).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let encodedUrl = URL(string: encodedUrlString) else { return nil }

        
        var request: URLRequest = URLRequest(url: encodedUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = method.rawValue
        
        if !body.isEmpty {
            request.httpBody = body.convertToData()
        }

        return request
    }
}

extension ServerRequesterCore {
    func execute<Response: Codable>(request: URLRequest, responseType: Response.Type,
                         success: @escaping(Response) -> (),
                         failure: @escaping(ApiError) -> ()) {
        
        Alamofire.request(request).responseData(completionHandler: { response in
            let statusCode = response.response?.statusCode ?? 0

            switch true {
            case response.result.isFailure: failure(.noInternet)
            case statusCode >= 300 && statusCode <= 500: failure(.unidentified)
            case statusCode >= 500: failure(.serverUnreachable)
            default:
                do {
                    let data = response.result.value ?? Data()
                    let response = try (JSONDecoder().decode(responseType, from: data))
                    success(response)
                } catch {
                    failure(.incorrectIncomingData)
                }
            }
        })
    }
}

