//
//  ServerRequests.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
}

class ServerRequests: WeatherApiService {
    func performWeatherRequest(parameters: [String: String],
                               success: @escaping(CityFullArray) -> (),
                               failure: @escaping(ApiError) -> ()) {
        
        let core = ServerRequesterCore()
        
        let urlString = Constants.baseUrlString + "data/2.5/group?"
        guard let request = core.constructRequest(urlString: urlString,
                                               parameters: parameters,
                                               method: .get,
                                               body: [:]) else { failure(.unidentified); return }
        
        core.execute(request: request, responseType: CityFullArray.self, success: success, failure: failure)
    }
}
