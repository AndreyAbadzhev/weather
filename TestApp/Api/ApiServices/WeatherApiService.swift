//
//  WeatherApiService.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
protocol WeatherApiService {
    func performWeatherRequest(parameters: [String: String],
                               success: @escaping(CityFullArray) -> (),
                               failure: @escaping(ApiError) -> ())
}
