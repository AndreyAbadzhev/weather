//
//  ApiError.swift
//  TestApp
//
//  Created by Андрей Абаджев on 13/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
enum ApiError: String {
    case conflict = "Please, check the data, you entered and try again"
    case noInternet = "Check your internet connection"
    case serverUnreachable = "Server is unreachable"
    case incorrectIncomingData = "Incorrect data"
    case unidentified = "Something went wrong"
}
