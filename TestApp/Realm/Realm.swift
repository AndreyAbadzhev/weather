//
//  Realm.swift
//  TestApp
//
//  Created by Андрей Абаджев on 07/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import RealmSwift


class RealmDataBase {
    var realm: Realm = try! Realm()
    
    func write<T: Object>(object: [T]) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    func obtain<T: Object>(objectType: T.Type) -> Results<T>? {
        return realm.objects(objectType)
    }
    
    func delete<T: Object>(objects: [T]) {
        try! realm.write {
            realm.delete(objects)
        }
    }
    
    deinit {
        print("Realm manager deallocated")
    }
}
