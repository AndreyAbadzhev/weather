//
//  SplashScreenViewController.swift
//  TestApp
//
//  Created by Андрей Абаджев on 16/08/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var label: UILabel!
    
    var viewDidAppear: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("splash screen screated")
        activityIndicator.alpha = 0
        label.alpha = 0
        activityIndicator.startAnimating()
        activityIndicator.animatedShow()
        label.animatedShow()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("splash Screen appeared")
        viewDidAppear?()
    }
    
    deinit {
        print("SC Splash Screen deallocated")
    }
}
