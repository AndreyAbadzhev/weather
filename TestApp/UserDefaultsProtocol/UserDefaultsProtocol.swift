//
//  DataStorageProtocol.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
protocol UserDefaultsProtocol {}

extension UserDefaultsProtocol {
    
    var _firstLaunchCompleted: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isFirstLaunchCompleted")
        }
    }
    
    var _chosenCitiesIds: [Int]? {
        get {
            if let array = UserDefaults.standard.array(forKey: "chosenCitiesIds") as? [Int] {
                return array
            } else {
                return nil
            }
        }
    }
    
    func _writeCitiesIds(array: [Int]) {
        UserDefaults.standard.set(array, forKey: "chosenCitiesIds")
    }
    
    func _firstLaunchSetCompleted() {
        UserDefaults.standard.set(true, forKey: "isFirstLaunchCompleted")
    }
}
