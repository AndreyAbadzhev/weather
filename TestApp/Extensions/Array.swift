//
//  Array.swift
//  TestApp
//
//  Created by Андрей Абаджев on 13/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
extension Array {
    func getElement(index: Int) -> Element? {
        if index >= 0 && index <= self.count - 1 {
            return self[index]
        } else {
            return nil
        }
    }
    
    func removeElement(index: Int) -> Array {
        if index >= 0 && index <= self.count - 1 {
            var array = self
            array.remove(at: index)
            return array
        }
        return self
    }
}
