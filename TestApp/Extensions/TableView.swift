//
//  TableView.swift
//  TestApp
//
//  Created by Андрей Абаджев on 14/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import UIKit
extension UITableView {
    func reloadData(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion: { _ in completion() } )
    }
    
    func animatedShow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1
        })
    }
    
    func animatedHide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        })
    }
}
