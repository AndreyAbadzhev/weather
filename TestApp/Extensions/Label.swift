//
//  Label.swift
//  TestApp
//
//  Created by Андрей Абаджев on 15/08/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import UIKit
extension UILabel {
    func animatedShow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1
        })
    }
    
    func animatedHide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        })
    }
}
