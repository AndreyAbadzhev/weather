//
//  ActivityIndicator.swift
//  TestApp
//
//  Created by Андрей Абаджев on 16/08/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import UIKit
extension UIActivityIndicatorView {
    func animatedShow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1
        })
    }
    
    func animatedHide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        })
    }
}
