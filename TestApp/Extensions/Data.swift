//
//  Data.swift
//  TestApp
//
//  Created by Andrey Abadzhev on 26/06/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation

extension Data {
    func parseJSON() -> NSDictionary? {
        do {
            return try JSONSerialization.jsonObject(with: self, options: []) as? NSDictionary
        } catch {
            return nil
        }
    }
}
