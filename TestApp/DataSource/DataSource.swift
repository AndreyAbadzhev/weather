//
//  DataSource.swift
//  TestApp
//
//  Created by Андрей Абаджев on 15/08/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
class DataSource {
    
    private static var instance: DataSource?
    
    private init() {}
    
    static func shared() -> DataSource {
        if instance == nil { instance = DataSource() }
        return instance!
    }
    
    var citiesArrayPreview: [CityShort] = []
    var citiesArray: [CityShort] = []
    var citiesSearchResult: [CityShort] = []
    
    deinit {
        print("SC Data Source deallocated")
    }
}
