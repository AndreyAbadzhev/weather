//
//  MVVMCitiesView.swift
//  TestApp
//
//  Created by Андрей Абаджев on 14/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import UIKit
import RxSwift
class MVVMCitiesView: UIViewController {

    @IBOutlet weak var noCitiesChosenLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = MVVMCitiesViewModel()
    let bag = DisposeBag()
 
    var cells: [CityFull]? {
        didSet {
            tableView.reloadData { self.tableView.refreshControl?.endRefreshing() }
            noCitiesChosenLabel.isHidden = cells?.count == 0 ? false : true
            tableView.isHidden = !noCitiesChosenLabel.isHidden
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.performServerRequest()
    }
    
    func setupInitialState() {
        title = "Cities"
        
        noCitiesChosenLabel.isHidden = true
        
        let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonDidTapped))
        navigationItem.rightBarButtonItem = addBarButton
        
        tableView.register(UINib(nibName: String(describing: CitiesCell.self), bundle: Bundle.main),
                           forCellReuseIdentifier: String(describing: CitiesCell.self))
        
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        
        viewModel.cells?.subscribe(onNext: { [weak self] in
            self?.cells = $0
        }).disposed(by: bag)
        
        viewModel.error?.subscribe(onNext: { [weak self] in
            self?.showErrorAlert(error: $0)
        }).disposed(by: bag)
    }
    
    @objc func refreshTableView() {
        viewModel.performServerRequest()
    }
    
    @objc func addButtonDidTapped() {
        let selectCitiesViewController = MVVMSelectCitiesView()
        navigationController?.pushViewController(selectCitiesViewController, animated: true)
    }
    
    func showErrorAlert(error: ApiError) {
        let alert = AlertConstructor().errorAlert(error: error)
        present(alert, animated: true, completion: nil)
    }
}

extension MVVMCitiesView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let city = cells?.getElement(index: indexPath.row) else { return UITableViewCell() }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CitiesCell.self)) as? CitiesCell else { return UITableViewCell() }
        cell.configure(city: city)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let cell = tableView.cellForRow(at: indexPath) as? CitiesCell else { return }
            guard let cityId = cell.city?.id else { return }
            viewModel.deleteCity.on(.next(cityId))
            cells?.remove(at: indexPath.row)
        }
    }
    
}
