//
//  MVVMCitiesViewModel.swift
//  TestApp
//
//  Created by Андрей Абаджев on 14/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import RxSwift
class MVVMCitiesViewModel {
    
    var dataSource: MVVMCitiesDataSource?
    let bag = DisposeBag()
    
    var cells: PublishSubject<[CityFull]>?
    var error: PublishSubject<ApiError>?
    
    let deleteCity = PublishSubject<Int>()
    
    init() {
        dataSource = MVVMCitiesDataSource(viewModel: self)
        
        cells = dataSource?.cells
        error = dataSource?.error
    }
    
    func performServerRequest() {
        dataSource?.performServerRequest()
    }
}
