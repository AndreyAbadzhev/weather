//
//  MVVMCitiesModel.swift
//  TestApp
//
//  Created by Андрей Абаджев on 14/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import RxSwift
class MVVMCitiesDataSource: UserDefaultsProtocol {
    
    let api: WeatherApiService = ServerRequests()
    let bag = DisposeBag()
    
    
    let cells = PublishSubject<[CityFull]>()
    let error = PublishSubject<ApiError>()
    
    init(viewModel: MVVMCitiesViewModel) {
        viewModel.deleteCity.subscribe(onNext: { [weak self] in
            self?.removeCity(cityId: $0)
        }).disposed(by: bag)
    }
    
    func performServerRequest() {
        
        guard let citiesIdsArray = _chosenCitiesIds, !citiesIdsArray.isEmpty else { self.cells.on(.next([])); return }
        let citiesIdsStringArray = citiesIdsArray.map { String($0) }
        let citiesIdsString = citiesIdsStringArray.reduce("", { $0 == "" ? $1 : $0 + "," + $1 } )
        
        let parameters = ["id": citiesIdsString, "units": "metric", "appid": Constants.openWeatherApiKey]
        
        api.performWeatherRequest(parameters: parameters, success: { [weak self] cityFullArray in
            self?.cells.on(.next(cityFullArray.array))
            }, failure: { [weak self] error in
               self?.error.on(.next(error))
        })
    }
    
    func removeCity(cityId: Int) {
        guard let chosenCitiesIds = _chosenCitiesIds else { return }
        let updatedChosenCitiesIds = chosenCitiesIds.filter { $0 != cityId }
        _writeCitiesIds(array: updatedChosenCitiesIds)
    }
}
