//
//  MWWMSelectCitiesDataSource.swift
//  TestApp
//
//  Created by Андрей Абаджев on 14/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
class MVVMSelectCitiesDataSource: UserDefaultsProtocol {

    weak var viewModel: MVVMSelectCitiesViewModel?
    let realm: RealmDataBase = RealmDataBase()
    let bag = DisposeBag()
    
    let cells = PublishSubject<[CityShort]>()
    
    var cities: [CityShort]? {
        didSet {
            cells.on(.next(self.cities ?? []))
        }
    }
    
    var searchResult: [CityShort]? {
        didSet {
            cells.on(.next(self.searchResult ?? []))
        }
    }
    
    var selectedCities: [Int]? {
        didSet {
            writeSelectedCitiesToDataStorage()
            viewModel?.citiesSelected.on(.next(selectedCities ?? []))
        }
    }
    
    init(viewModel: MVVMSelectCitiesViewModel) {
        self.viewModel = viewModel
        selectedCities = _chosenCitiesIds ?? []
        
        viewModel.celectCity.subscribe(onNext: { [weak self] in
            let id = $0
            if self?.selectedCities?.contains(id) == true {
                self?.selectedCities = self?.selectedCities?.filter { $0 != id } ?? []
            } else {
                self?.selectedCities?.append(id)
            }
        }).disposed(by: bag)
        
        
        viewModel.searchText.subscribe(onNext: { [weak self] in
            let searchText = $0
            self?.searchResult = self?.cities?.filter { $0.name.contains(searchText) }
        }).disposed(by: self.bag)
    }
    
    func loadShotCitiesFromRealm() {
       /* if let array = realm.obtain(objectType: CityShort.self), array.count != 0 {
            self.cities = array.map { $0 }
        } else {
            self.cities = []
        }*/
    }
    
    func recoverSelectedCitiesFromDataStorage() {
        selectedCities = _chosenCitiesIds
    }
    
    func writeSelectedCitiesToDataStorage() {
        _writeCitiesIds(array: selectedCities ?? [])
    }
    
    deinit {
        print("MVVM Select Cities DS deallocated")
    }
    
}
