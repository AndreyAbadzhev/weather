//
//  MWWMSelectCitiesViewModel.swift
//  TestApp
//
//  Created by Андрей Абаджев on 14/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import Foundation
import RxSwift
class MVVMSelectCitiesViewModel: UserDefaultsProtocol {
    
    var dataSource: MVVMSelectCitiesDataSource?
    let bag = DisposeBag()
    
    var cells: PublishSubject<[CityShort]>?

    var celectCity = PublishSubject<Int>()
    var citiesSelected = PublishSubject<[Int]>()
    var searchText = PublishSubject<String>()
    
    init() {
        dataSource = MVVMSelectCitiesDataSource(viewModel: self)
        cells = dataSource?.cells
    }
    
    func checkForSelection(cityId: Int) -> Bool {
        if  _chosenCitiesIds?.contains(cityId) == true {
            return true
        } else {
            return false
        }
    }
    
    func loadShotCitiesFromRealm() {
        dataSource?.loadShotCitiesFromRealm()
    }
    
    func recoverSelectedCitiesFromDataStorage() {
        dataSource?.recoverSelectedCitiesFromDataStorage()
    }
    
    deinit {
        print("MVVM Select Cities VM deallocated")
    }
    
}
