//
//  MWWMSelectCitiesView.swift
//  TestApp
//
//  Created by Андрей Абаджев on 14/07/2019.
//  Copyright © 2019 Andrey Abadzhev. All rights reserved.
//

import UIKit
import RxSwift
class MVVMSelectCitiesView: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var cityInputSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var doneBarButton: UIBarButtonItem?

    let viewModel = MVVMSelectCitiesViewModel()
    let bag = DisposeBag()
    
    var cells: [CityShort]? {
        didSet {
            tableView.reloadData()
            tableView.alpha = 1
            cityInputSearchBar.alpha = 1
            activityIndicator.isHidden = true
        }
    }
    
    var selectedCities: [Int]? {
        didSet {
            guard let cells = tableView.visibleCells as? [ShortCityCell] else { return }
            for cell in cells {
                cell.accessoryType = selectedCities?.contains(cell.cityId) == true ? .checkmark : .none
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.loadShotCitiesFromRealm()
        viewModel.recoverSelectedCitiesFromDataStorage()
    }
    
    func setupInitialState() {
        title = "Select City"
        activityIndicator.startAnimating()

        tableView.alpha = 0
        cityInputSearchBar.alpha = 0
        tableView.register(UINib(nibName: String(describing: ShortCityCell.self), bundle: Bundle.main), forCellReuseIdentifier: String(describing: ShortCityCell.self))
        
        
        viewModel.cells?.subscribe(onNext: { [weak self] in
            self?.cells = $0
        }).disposed(by: bag)
        
        viewModel.citiesSelected.subscribe(onNext: { [weak self] in
            self?.selectedCities = $0
        }).disposed(by: bag)
    }
    
    deinit {
        print("MVVM Select Cities VC deallocated")
    }
}

extension MVVMSelectCitiesView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let city = cells?.getElement(index: indexPath.row) else { return UITableViewCell() }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ShortCityCell.self)) as? ShortCityCell else { return UITableViewCell() }
        cell.configure(cityShort: city)
        cell.accessoryType = selectedCities?.contains(cell.cityId) == true ? .checkmark : .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ShortCityCell else { return }
        doneBarButton?.isEnabled = true
        let cityId = cell.cityId
        viewModel.celectCity.on(.next(cityId))
    }
}

extension MVVMSelectCitiesView: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //output?.searchInProgress = false
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = ""
        searchBar.resignFirstResponder()
        //updateViewState()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        activityIndicator.isHidden = false
        
        if searchBar.text != "" {
            viewModel.searchText.on(.next(searchBar.text ?? ""))
        } else {
            tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
